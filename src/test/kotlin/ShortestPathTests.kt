import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class ShortestPathTests {

    @Test
    fun `the source row is less than zero`() {
        val shortestPath = ShortestPath(convertToArray("00\n00"))
        assertThrows<IllegalArgumentException> {
            shortestPath.solve(-1, 0, 1, 1)
        }
    }

    @Test
    fun `the source row is bigger than the number of rows`() {
        val shortestPath = ShortestPath(convertToArray("00\n00"))
        assertThrows<IllegalArgumentException> {
            shortestPath.solve(2, 0, 1, 1)
        }
    }

    @Test
    fun `the source column is less than zero`() {
        val shortestPath = ShortestPath(convertToArray("00\n00"))
        assertThrows<IllegalArgumentException> {
            shortestPath.solve(0, -1, 1, 1)
        }
    }

    @Test
    fun `the source column is bigger than the number of columns`() {
        val shortestPath = ShortestPath(convertToArray("00\n00"))
        assertThrows<IllegalArgumentException> {
            shortestPath.solve(0, 2, 1, 1)
        }
    }

    @Test
    fun `the target row is less than zero`() {
        val shortestPath = ShortestPath(convertToArray("00\n00"))
        assertThrows<IllegalArgumentException> {
            shortestPath.solve(0, 0, -1, 1)
        }
    }

    @Test
    fun `the target row is bigger than the number of rows`() {
        val shortestPath = ShortestPath(convertToArray("00\n00"))
        assertThrows<IllegalArgumentException> {
            shortestPath.solve(0, 0, 1, 2)
        }
    }

    @Test
    fun `the target column is less than zero`() {
        val shortestPath = ShortestPath(convertToArray("00\n00"))
        assertThrows<IllegalArgumentException> {
            shortestPath.solve(0, 0, 1, -1)
        }
    }

    @Test
    fun `the target column is bigger than the number of columns`() {
        val shortestPath = ShortestPath(convertToArray("00\n00"))
        assertThrows<IllegalArgumentException> {
            shortestPath.solve(0, 0, 1, 2)
        }
    }

    @Test
    fun `a map without rows`() {
        assertThrows<IllegalArgumentException> {
            test("", "", 0, 0, 0, 0)
        }
    }

    @Test
    fun `a map without columns`() {
        val map = Array(5) { IntArray(0) }
        assertThrows<IllegalArgumentException> {
            ShortestPath(map)
        }
    }

    @Test
    fun `the minimal map`() {
        test("0", "X", 0, 0, 0, 0)
    }

    @Test
    fun `a tiny map`() {
        test(
            """
            00
            00
            """.trimIndent(),
            """
            XX
            0X
            """.trimIndent(),
            0, 0, 1, 1
        )
    }

    @Test
    fun `a simple map`() {
        test(
            """
            00000000
            00009900
            00000900
            00000000
            00000000
            """.trimIndent(),
            """
            0XXXXXX0
            0X009900
            0X000900
            0X000000
            0X000000
            """.trimIndent(),
            4, 1, 0, 6
        )
    }

    @Test
    fun `a room with a door`() {
        test(
            """
            00000000
            0******0
            0*0000*0
            0***00*0
            00000000
            """.trimIndent(),
            """
            XXX00000
            X******0
            X*XXX0*0
            X***X0*0
            XXXXX000
            """.trimIndent(),
            2, 2, 0, 2
        )
    }

    @Test
    fun `a room with closed doors`() {
        test(
            """
            11111111
            11***9*1
            1*1111*1
            6****1*1
            11111111
            """.trimIndent(),
            """
            1XXXXX11
            1X***X*1
            1*XXXX*1
            6****1*1
            11111111
            """.trimIndent(),
            2, 2, 1, 1
        )
    }

    @Test
    fun `a big map`() {
        test(
            """
            1111111111111*111111111*111111111111
            *********1111*******1******1*******1
            11*111*1*1111111111111111111111111*1
            11*1*111*11111111111111*111111*****1
            11*1**************1********1***11111
            11*111111111*11111111111111*1**111*1
            11*********1*11111111*****1*1******1
            11*111111111*11111111*111*1*111111*1
            11***11**************1111***11111111
            11*11111111111111**11111111*111111*1
            11*111111111111*1*111111111**1*****1
            11********11111*11111111*****1*11111
            111111111*******11111111111111*11111
            **1111111*11111*******11*11111*11111
            111********1111111111*1**11111******
            111111111111111*11111*11****11*11111
            ****************11111******111*11111
            111111111111111111111111111111111111
            """.trimIndent(),
            """
            XXXXXXXXXX111*111111111*111XXXXXXXXX
            *********X111*******1******X*******X
            X1*111*1*XXXXXXXXXXXXXXXXXXX111111*X
            X1*1*111*11111111111111*111111*****X
            X1*1**************1********1***1111X
            X1*111111111*11111111111111*1**111*X
            X1*********1*11111111*****1*1******X
            X1*111111111*11111111*111*1*111111*X
            X1***11**************1111***1XXXXXXX
            X1*11111111111111**11111111*1X1111*1
            X1*111111111111*1*111111111**X*****1
            X1********11111*11111111*****X*11111
            XXX111111*******111111111111XX*11111
            **X111111*11111*******11*111X1*11111
            11X********XXXXXX1111*1**111X1******
            11XXXXXXXXXX111*X1111*11****X1*11111
            ****************X1111******XX1*11111
            1111111111111111XXXXXXXXXXXX11111111
            """.trimIndent(),
            0, 0, 2, 0
        )
    }

    private fun test(
        map: String,
        expectedPath: String,
        sourceRow: Int,
        sourceColumn: Int,
        targetRow: Int,
        targetColumn: Int
    ) {
        val arrayMap = convertToArray(map)
        val shortestPath = ShortestPath(arrayMap)

        val path = shortestPath.solve(sourceRow, sourceColumn, targetRow, targetColumn)
        val result = StringBuffer(map)
        for (cell in path)
            result.setCharAt(cell.row * (arrayMap[0].size + 1) + cell.column, 'X') // +1 - \n

        Assertions.assertEquals(result.toString(), expectedPath)
    }

    companion object {
        private fun convertToArray(map: String): Array<IntArray> {
            val rows = map.split('\n')
            val array = Array(rows.size) { IntArray(rows[0].length) }
            for (rowIndex in 0 until rows.size)
                for (columnIndex in 0 until rows[rowIndex].length) {
                    val char = rows[rowIndex][columnIndex]
                    val value = if (char == '*') 500 else char - '0'
                    array[rowIndex][columnIndex] = value
                }
            return array
        }
    }
}