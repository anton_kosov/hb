import java.util.*

class ShortestPath(map: Array<IntArray>) {
    private val mMap = map
    private val mRows: Int
    private val mColumns: Int

    init {
        require(map.isNotEmpty()) { "The map must have rows." }
        require(map[0].isNotEmpty()) { "The map must have columns." }
        mRows = mMap.size
        mColumns = mMap[0].size
    }

    fun solve(sourceRow: Int, sourceColumn: Int, targetRow: Int, targetColumn: Int): List<Cell> {

        require(sourceColumn in 0 until mColumns) { "sourceColumn is out of range." }
        require(sourceRow in 0 until mRows) { "sourceRow is out of range." }
        require(targetColumn in 0 until mColumns) { "targetColumn is out of range." }
        require(targetRow in 0 until mRows) { "targetRow is out of range." }

        val sourceCell = Cell(sourceRow, sourceColumn)
        val targetCell = Cell(targetRow, targetColumn)
        val infinity = Int.MAX_VALUE

        val processedCells = Array(mRows) { BooleanArray(mColumns) { false } }

        val delays = Array(mRows) { IntArray(mColumns) { infinity } }
        delays[sourceCell] = 0

        val score = Array(mRows) { IntArray(mColumns) { infinity } }
        score[sourceCell] = sourceCell.distanceTo(targetCell)

        val previousCells: Array<Array<Cell?>> = Array(mRows) { Array<Cell?>(mColumns) { null } }

        val queueOfCells: PriorityQueue<Cell> = PriorityQueue(
            kotlin.Comparator { c1: Cell, c2: Cell -> score[c1] - score[c2] }
        )

        queueOfCells.add(sourceCell)
        while (queueOfCells.isNotEmpty()) {
            val currentCell = queueOfCells.poll()
            processedCells[currentCell] = true
            if (currentCell == targetCell) break // the shortest path has been found
            val currentDelay = delays[currentCell]
            for (adjacentCell in adjacentCells(currentCell)) {
                if (processedCells[adjacentCell]) continue

                val delay = currentDelay + mMap[adjacentCell]
                if (delay >= delays[adjacentCell]) continue

                delays[adjacentCell] = delay
                score[adjacentCell] = delay + adjacentCell.distanceTo(targetCell)
                previousCells[adjacentCell] = currentCell
                queueOfCells.add(adjacentCell)
            }
        }

        // Build the path
        val path = mutableListOf<Cell>()
        var currentCell = targetCell
        while (currentCell != sourceCell) {
            path.add(currentCell)
            currentCell = previousCells[currentCell]!!
        }
        path.add(sourceCell)

        /*// Output the score matrix
        println()
        for (rowIndex in 0 until score.size) {
            for (columnIndex in 0 until score[rowIndex].size) {
                val s = score[rowIndex][columnIndex]
                var o = if (s == infinity) "*" else s.toString()
                if (path.contains(Cell(rowIndex, columnIndex)))
                    o += "X"
                o = o.padStart(4, ' ')
                print("$o\t")
            }
            println()
        }*/

        return path
    }

    private fun adjacentCells(cell: Cell) = sequence {
        if (cell.column > 0)
            yield(Cell(cell.row, cell.column - 1))
        if (cell.row > 0)
            yield(Cell(cell.row - 1, cell.column))
        if (cell.column < mColumns - 1)
            yield(Cell(cell.row, cell.column + 1))
        if (cell.row < mRows - 1)
            yield(Cell(cell.row + 1, cell.column))
    }

    companion object {
        private operator fun Array<IntArray>.get(cell: Cell): Int = this[cell.row][cell.column]

        private operator fun Array<IntArray>.set(cell: Cell, value: Int) {
            this[cell.row][cell.column] = value
        }

        private operator fun Array<BooleanArray>.get(cell: Cell): Boolean = this[cell.row][cell.column]

        private operator fun Array<BooleanArray>.set(cell: Cell, value: Boolean) {
            this[cell.row][cell.column] = value
        }

        private operator fun Array<Array<Cell?>>.get(cell: Cell) = this[cell.row][cell.column]

        private operator fun Array<Array<Cell?>>.set(cell: Cell, value: Cell) {
            this[cell.row][cell.column] = value
        }
    }

    data class Cell(val row: Int, val column: Int) {
        fun distanceTo(cell: Cell): Int = Math.abs(row - cell.row) + Math.abs(column - cell.column)
    }
}